# Big Skinny Website
This website was part of a static web design/development project for 67-250. The goal of the project was to develop a restructuring of the current [Big Skinny site](http://bigskinny.net/).

**The project is deployed [here](https://liao-frank.github.io/Big-Skinny-Wallets/) through ghpages.**

## Built With

* [Foundations CSS](http://foundation.zurb.com/) - CSS Framework for grid system
